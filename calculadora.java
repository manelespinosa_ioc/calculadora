package ioc.xtec.cat.calculadora;

import java.util.Scanner;

/**
 * La classe Calculadora proporciona una interfície per realitzar operacions bàsiques 
 * com sumar dos números introduïts per l'usuari.
 * 
 * @author Manel Espinosa
 * @version 1.0
 */
public class Calculadora {

    /**
     * El mètode principal que s'executa en iniciar el programa.
     * Demana a l'usuari dos números i mostra els resultats de les operacions de suma amb aquests números.
     * 
     * @param args Els arguments de la línia de comandes.
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Introdueix el primer número: ");
        double num1 = scanner.nextDouble();

        System.out.println("Introdueix el segon número: ");
        double num2 = scanner.nextDouble();

        System.out.println("Suma: " + sumar(num1, num2));
        System.out.println("Resta: " + restar(num1, num2));
	System.out.println("Multiplicar: " + multiplicar(num1, num2));
	 try {
            System.out.println("Divisió: " + dividir(num1, num2));
        } catch (ArithmeticException e) {
            System.out.println("Error: " + e.getMessage());
        } finally {
            scanner.close();
        }

    }
    
    public static double sumar(double a, double b) {
        return a + b;
    }
    
    public static double restar(double a, double b) {
        return a - b;
    }
    public static double multiplicar(double a, double b) {
        return a * b;
    }
    public static double dividir(double a, double b) {
         if (b == 0) {
            throw new ArithmeticException("El divisor no pot ser zero. Manel Espinosa");
        }
        return a / b;
    }

}
